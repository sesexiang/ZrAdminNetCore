using System;
using ZR.Model.Models;

namespace ZR.Service.Business
{
    /// <summary>
    /// 代码生成演示service接口
    ///
    /// @author zr
    /// @date 2021-09-27
    /// </summary>
    public interface IGendemoService: IBaseService<Gendemo>
    {
        void Test();
    }
}
