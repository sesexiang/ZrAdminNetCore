﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    /// <summary>
    /// 全局静态常量
    /// </summary>
    public class GlobalConstant
    {
        /// <summary>
        /// 管理员权限
        /// </summary>
        public static string AdminPerm = "*:*:*";
    }
}
